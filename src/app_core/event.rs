use sdl2::{keyboard::Keycode, mouse::MouseButton};

#[derive(Debug)]
pub struct MouseEvent {
    button: MouseButton,
}

#[derive(Debug)]
pub enum KeyState {
    Up,
    Down(bool),
}

#[derive(Debug)]
pub struct KeyboardEvent {
    pub state: KeyState,
    pub keycode: Keycode,
}

#[derive(Debug)]
pub struct WindowEvent {}

#[derive(Debug)]
pub enum Event {
    Mouse(MouseEvent),
    Keyboard(KeyboardEvent),
    Window(WindowEvent),
}

use bevy_ecs::prelude::*;

use crate::{
    aabb::AABB,
    vulkan::{sprite_batch::Sprite, texture_region::TextureRegion},
};
#[derive(Component)]
pub struct SpriteComponent {
    pub sprite: Sprite<TextureRegion>,
}

#[derive(Component)]
pub struct PositionComponent {
    pub pos: glam::Vec2,
}

#[derive(Component)]
pub struct VelocityComponent {
    pub vel: glam::Vec2,
}

#[derive(Component)]
pub struct CollisionComponent {
    pub bounds: AABB,
}

#[derive(Component)]
pub struct CameraTagComponent {}

#[derive(Component)]
pub struct ControlComponent {}

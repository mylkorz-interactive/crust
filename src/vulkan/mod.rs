use thiserror::Error;

pub mod buffer;
pub mod context;
pub mod device;
pub mod sprite_batch;
pub mod swapchain;
pub mod texture;
pub mod texture_region;

#[derive(Error, Debug)]
pub enum VulkanError {
    #[error("Failed to load Vulkan context: {0}")]
    LoadError(String),
    #[error("Vulkan device error: {0}")]
    DeviceError(String),
    #[error("Vulkan swapchain error: {0}")]
    SwapchainError(String),

    #[error("Size cannot be 0")]
    InvalidSize,

    #[error("Vulkan API Error {0}")]
    ApiError(#[from] ash::vk::Result),
}

use crate::app_core::window::Window;
use crate::vulkan::device::Device;
use crate::vulkan::VulkanError;
use ash::extensions::*;
use ash::*;
use log::*;

pub struct Swapchain {
    pub surface_loader: khr::Surface,
    pub surface: vk::SurfaceKHR,
    pub extent: vk::Extent2D,
    pub surface_format: vk::SurfaceFormatKHR,
    pub image_views: Vec<vk::ImageView>,
    pub swapchain_handle: vk::SwapchainKHR,
    swapchain_loader: khr::Swapchain,
}

impl Swapchain {
    pub fn new(
        instance: &Instance,
        window: &Window,
        device: &Device,
        surface_loader: khr::Surface,
        surface: vk::SurfaceKHR,
    ) -> Result<Self, VulkanError> {
        info!("Creating swapchain");
        let surface_capabilities = unsafe {
            surface_loader
                .get_physical_device_surface_capabilities(device.physical_device, surface)
                .map_err(|e| VulkanError::SwapchainError(e.to_string()))?
        };

        let surface_formats = unsafe {
            surface_loader
                .get_physical_device_surface_formats(device.physical_device, surface)
                .map_err(|e| VulkanError::SwapchainError(e.to_string()))?
        };

        let present_modes = unsafe {
            surface_loader
                .get_physical_device_surface_present_modes(device.physical_device, surface)
                .map_err(|e| VulkanError::SwapchainError(e.to_string()))?
        };

        let surface_format = surface_formats
            .iter()
            .find(|format| {
                format.format == vk::Format::B8G8R8A8_UNORM
                    && format.color_space == vk::ColorSpaceKHR::SRGB_NONLINEAR
            })
            .or_else(|| surface_formats.get(0))
            .expect("Could not find a suitable surface format");

        let present_mode = present_modes
            .into_iter()
            .find(|&mode| mode == vk::PresentModeKHR::MAILBOX)
            .or_else(|| Some(vk::PresentModeKHR::FIFO))
            .expect("Could not find a suitable present mode");

        let swapchain_extent = match surface_capabilities.current_extent {
            vk::Extent2D {
                width: u32::MAX,
                height: u32::MAX,
            } => vk::Extent2D {
                width: window.get_width(),
                height: window.get_height(),
            },
            other => other,
        };

        let mut swapchain_image_count = surface_capabilities.min_image_count + 1;
        if surface_capabilities.max_image_count > 0
            && swapchain_image_count > surface_capabilities.max_image_count
        {
            swapchain_image_count = surface_capabilities.max_image_count;
        }

        let swapchain_create_info = vk::SwapchainCreateInfoKHR::builder()
            .min_image_count(swapchain_image_count)
            .image_format(surface_format.format)
            .image_color_space(surface_format.color_space)
            .image_extent(swapchain_extent)
            .image_array_layers(1)
            .image_usage(vk::ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(vk::SharingMode::EXCLUSIVE)
            .pre_transform(surface_capabilities.current_transform)
            .composite_alpha(vk::CompositeAlphaFlagsKHR::OPAQUE)
            .present_mode(present_mode)
            .clipped(true)
            .old_swapchain(vk::SwapchainKHR::null())
            .surface(surface);

        let swapchain_loader = khr::Swapchain::new(instance, &device.logical_device);

        info!(
            "Swapchain properties:
            Present mode: {:?}
            Surface format: {:?}
            Surface color space: {:?}
            Surface extent: {:?}",
            present_mode, surface_format.format, surface_format.color_space, swapchain_extent
        );

        let swapchain = unsafe {
            swapchain_loader
                .create_swapchain(&swapchain_create_info, None)
                .map_err(|e| VulkanError::SwapchainError(e.to_string()))?
        };

        let swapchain_images = unsafe {
            swapchain_loader
                .get_swapchain_images(swapchain)
                .map_err(|e| VulkanError::SwapchainError(e.to_string()))?
        };
        let swapchain_image_views = swapchain_images
            .into_iter()
            .map(|image| {
                let image_view_info = vk::ImageViewCreateInfo::builder()
                    .image(image)
                    .view_type(vk::ImageViewType::TYPE_2D)
                    .format(surface_format.format)
                    .components(vk::ComponentMapping {
                        r: vk::ComponentSwizzle::IDENTITY,
                        g: vk::ComponentSwizzle::IDENTITY,
                        b: vk::ComponentSwizzle::IDENTITY,
                        a: vk::ComponentSwizzle::IDENTITY,
                    })
                    .subresource_range({
                        vk::ImageSubresourceRange::builder()
                            .aspect_mask(vk::ImageAspectFlags::COLOR)
                            .base_mip_level(0)
                            .level_count(1)
                            .base_array_layer(0)
                            .layer_count(1)
                            .build()
                    });

                unsafe {
                    device
                        .logical_device
                        .create_image_view(&image_view_info, None)
                        .expect("Could not create swapchain image view")
                }
            })
            .collect();

        Ok(Swapchain {
            surface_loader,
            surface,
            extent: swapchain_extent,
            surface_format: *surface_format,
            image_views: swapchain_image_views,
            swapchain_handle: swapchain,
            swapchain_loader,
        })
    }

    pub fn acquire_next_image(&self, semaphore: vk::Semaphore, fence: vk::Fence) -> (u32, bool) {
        if let Ok((index, state)) = unsafe {
            self.swapchain_loader.acquire_next_image(
                self.swapchain_handle,
                u64::MAX,
                semaphore,
                fence,
            )
        } {
            return (index, state);
        }

        (0, false)
    }

    pub fn queue_present(&self, queue: vk::Queue, present_info: &vk::PresentInfoKHR) {
        let _ = unsafe { self.swapchain_loader.queue_present(queue, present_info) };
    }
}

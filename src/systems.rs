use std::sync::{Arc, Mutex};

use bevy_ecs::prelude::*;
use sdl2::keyboard::Keycode;

use crate::app_core::context::Context;
use crate::components::*;
use crate::gfx::camera::OrthoCamera;
use crate::vulkan::sprite_batch::SpriteBatch;

pub fn render_system(batch: Res<Arc<Mutex<SpriteBatch>>>, query: Query<&SpriteComponent>) {
    let mut batch = batch.lock().unwrap();

    for c in query.iter() {
        batch.draw_sprite(&c.sprite);
    }
}

pub fn velocity_system(
    delta_time: Res<f32>,
    mut query: Query<(&mut PositionComponent, &VelocityComponent)>,
) {
    for (mut p, v) in query.iter_mut() {
        p.pos += v.vel * delta_time.clone();
    }
}

pub fn sprite_position_system(mut query: Query<(&mut SpriteComponent, &PositionComponent)>) {
    for (mut s, p) in query.iter_mut() {
        s.sprite.set_position(p.pos);
    }
}

pub fn control_system(
    app_context: Res<Arc<Context>>,
    mut query: Query<(&mut VelocityComponent, &ControlComponent)>,
) {
    for (mut v, _) in query.iter_mut() {
        v.vel = glam::vec2(0.0, 0.0);

        let speed = 200.0;

        if app_context.is_key_pressed(Keycode::W) {
            v.vel.y = -speed;
        }

        if app_context.is_key_pressed(Keycode::S) {
            v.vel.y = speed;
        }

        if app_context.is_key_pressed(Keycode::D) {
            v.vel.x = speed;
        }

        if app_context.is_key_pressed(Keycode::A) {
            v.vel.x = -speed;
        }
    }
}

pub fn camera_tag_system(
    mut camera: ResMut<OrthoCamera>,
    query: Query<(&CameraTagComponent, &PositionComponent)>,
) {
    for (_, p) in query.iter() {
        camera.set_position(p.pos);
    }
}

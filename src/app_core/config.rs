pub struct Config {
    pub window_title: String,
    pub window_width: u32,
    pub window_height: u32,
    pub enable_validation: bool,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            window_title: "Untitled".to_string(),
            window_width: 1280,
            window_height: 600,
            enable_validation: true,
        }
    }
}

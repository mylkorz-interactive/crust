use super::{sprite_batch::Batchable, texture::Texture};

#[derive(Clone)]
pub struct TextureRegion {
    texture: Texture,

    region: (u32, u32, u32, u32),
    uvs: (f32, f32, f32, f32),
}

impl TextureRegion {
    pub fn split_from_texture(
        texture: Texture,
        region_width: u32,
        region_height: u32,
    ) -> Vec<Self> {
        let mut regions = Vec::new();

        let dims = texture.get_dimensions();

        let cols = dims.0 / region_width;
        let rows = dims.1 / region_height;

        for y in 0..rows {
            for x in 0..cols {
                let mut region = TextureRegion::new(texture.clone());
                region.set_region(
                    x * region_width,
                    y * region_height,
                    region_width,
                    region_height,
                );

                regions.push(region);
            }
        }

        regions
    }

    pub fn new(texture: Texture) -> Self {
        let dims = texture.get_dimensions();

        TextureRegion {
            texture,
            region: (0, 0, dims.0, dims.1),
            uvs: (0.0, 0.0, 1.0, 1.0),
        }
    }

    pub fn set_texture(&mut self, texture: Texture) {
        self.texture = texture;

        let dims = self.texture.get_dimensions();
        self.region = (0, 0, dims.0, dims.1);
        self.update_uvs();
    }

    pub fn get_texture(&self) -> Texture {
        self.texture.clone()
    }

    pub fn get_uvs(&self) -> (f32, f32, f32, f32) {
        self.uvs
    }

    pub fn get_region(&self) -> (u32, u32, u32, u32) {
        self.region
    }

    pub fn set_region(&mut self, x: u32, y: u32, w: u32, h: u32) {
        let dims = self.texture.get_dimensions();

        let origin = (x.min(dims.0), y.min(dims.1));
        let extent = ((origin.0 + w).min(dims.0), (origin.1 + h).min(dims.1));

        self.region = (origin.0, origin.1, extent.0, extent.1);
        self.update_uvs();
    }

    pub fn flip_x(&mut self) {}

    pub fn flip_y(&mut self) {}

    fn update_uvs(&mut self) {
        let dims = self.texture.get_dimensions();

        self.uvs = (
            self.region.0 as f32 / dims.0 as f32,
            self.region.1 as f32 / dims.1 as f32,
            self.region.2 as f32 / dims.0 as f32,
            self.region.3 as f32 / dims.1 as f32,
        );
    }
}

impl Batchable for TextureRegion {
    fn get_texture(&self) -> Texture {
        self.texture.clone()
    }

    fn get_uvs(&self) -> (f32, f32, f32, f32) {
        self.uvs
    }

    fn get_size(&self) -> (u32, u32) {
        (self.region.2 - self.region.0, self.region.3 - self.region.1)
    }
}

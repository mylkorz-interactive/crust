use std::{
    collections::HashSet,
    hash::Hash,
    sync::{Arc, RwLock},
};

use sdl2::{keyboard::Keycode, mouse::MouseButton};

use super::window::Window;

pub struct Context {
    pub vulkan_context: Arc<crate::vulkan::context::Context>,

    pub window: Window,
    pub(crate) pressed_keys: RwLock<HashSet<Keycode>>,
    pub(crate) pressed_buttons: RwLock<HashSet<MouseButton>>,
}

impl Context {
    pub fn get_pressed_keys(&self) -> HashSet<Keycode> {
        self.pressed_keys.read().unwrap().clone()
    }

    pub fn is_key_pressed(&self, key: Keycode) -> bool {
        self.pressed_keys.read().unwrap().contains(&key)
    }

    pub fn get_pressed_buttons(&self) -> HashSet<MouseButton> {
        self.pressed_buttons.read().unwrap().clone()
    }

    pub fn is_button_pressed(&self, button: MouseButton) -> bool {
        self.pressed_buttons.read().unwrap().contains(&button)
    }
}

unsafe impl Sync for Context {}
unsafe impl Send for Context {}

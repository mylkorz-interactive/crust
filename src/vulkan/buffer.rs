use crate::vulkan::{context::Context, VulkanError};
use ash::{vk::MemoryPropertyFlags, *};
use std::{os::raw::c_void, ptr, sync::Arc};
use vk_mem::*;

pub(crate) struct VkBuffer {
    context: Arc<Context>,

    handle: ash::vk::Buffer,

    allocation: Allocation,
    allocation_info: AllocationInfo,

    memory_property_flags: vk::MemoryPropertyFlags,
}

pub struct Buffer {
    pub(crate) inner: Arc<VkBuffer>,
}

impl Buffer {
    pub fn new(
        context: Arc<Context>,
        buffer_size: u64,
        usage_flags: vk::BufferUsageFlags,
        memory_usage: vk_mem::MemoryUsage,
        memory_flags: vk::MemoryPropertyFlags,
    ) -> Result<Self, VulkanError> {
        if buffer_size == 0 {
            return Err(VulkanError::InvalidSize);
        }

        let vk = context;

        let create_info = vk::BufferCreateInfo::builder()
            .size(buffer_size)
            .usage(usage_flags);

        let (buffer, allocation, allocation_info) = unsafe {
            let allocation_info = AllocationCreateInfo {
                usage: memory_usage,
                flags: AllocationCreateFlags::NONE,
                required_flags: memory_flags,
                preferred_flags: MemoryPropertyFlags::empty(),
                memory_type_bits: 0,
                pool: None,
                user_data: None,
            };

            vk.memory_allocator
                .create_buffer(&create_info, &allocation_info)?
        };

        let buffer = VkBuffer {
            context: vk,
            handle: buffer,
            allocation,
            allocation_info,
            memory_property_flags: memory_flags,
        };

        Ok(Buffer {
            inner: Arc::new(buffer),
        })
    }

    pub fn map_memory(&self) -> Option<*mut u8> {
        let ptr = self.inner.allocation_info.get_mapped_data();

        if ptr == ptr::null_mut() {
            if let Ok(data) = unsafe {
                self.inner
                    .context
                    .memory_allocator
                    .map_memory(self.inner.allocation)
            } {
                return Some(data);
            }

            return None;
        }

        Some(ptr)
    }

    pub fn unmap_memory(&self) {
        unsafe {
            self.inner
                .context
                .memory_allocator
                .unmap_memory(self.inner.allocation)
        }
    }

    pub fn flush_memory_range(&self, offset: usize, size: usize) -> Result<(), VulkanError> {
        unsafe {
            self.inner.context.memory_allocator.flush_allocation(
                self.inner.allocation,
                offset,
                size,
            )?
        }

        Ok(())
    }

    pub fn flush_memory(&self) -> Result<(), VulkanError> {
        self.flush_memory_range(0, self.inner.allocation_info.get_size())
    }

    pub fn copy_data_with_offset<T>(
        &self,
        data: &[T],
        dst_offset: usize,
    ) -> Result<(), VulkanError> {
        self.copy_ptr_with_offset(
            data.as_ptr() as *const c_void,
            std::mem::size_of::<T>() * data.len(),
            dst_offset,
        )
    }

    pub fn copy_data<T>(&self, data: &[T]) -> Result<(), VulkanError> {
        self.copy_data_with_offset(data, 0)
    }

    pub fn copy_ptr_with_offset(
        &self,
        src_ptr: *const c_void,
        src_size: usize,
        dst_offset: usize,
    ) -> Result<(), VulkanError> {
        let copy_len = std::cmp::min(src_size, self.inner.allocation_info.get_size());

        // If memory supports mapping
        if self
            .inner
            .memory_property_flags
            .contains(vk::MemoryPropertyFlags::HOST_VISIBLE)
        {
            let coherent_memory = self
                .inner
                .memory_property_flags
                .contains(vk::MemoryPropertyFlags::HOST_COHERENT);

            let ptr = self.map_memory();

            if let Some(ptr) = ptr {
                unsafe {
                    ptr::copy_nonoverlapping(src_ptr, ptr.add(dst_offset) as *mut c_void, copy_len);

                    // If the memory is not coherent, we need to flush the range to make the changes available to the device
                    if !coherent_memory {
                        self.flush_memory_range(dst_offset, copy_len)?;
                    }
                }

                self.unmap_memory();
            }
        // If it does not support mapping, we cry in a corner because we need a lot of stuff for this
        } else {
            let staging_buffer_info = vk::BufferCreateInfo::builder()
                .size(copy_len as u64)
                .usage(vk::BufferUsageFlags::TRANSFER_SRC);

            let allocation_info = AllocationCreateInfo {
                usage: MemoryUsage::CpuOnly,
                flags: AllocationCreateFlags::NONE,
                required_flags: MemoryPropertyFlags::empty(),
                preferred_flags: MemoryPropertyFlags::empty(),
                memory_type_bits: 0,
                pool: None,
                user_data: None,
            };

            let (buffer, allocation, _allocation_info) = unsafe {
                self.inner
                    .context
                    .memory_allocator
                    .create_buffer(&staging_buffer_info, &allocation_info)?
            };

            unsafe {
                let dst_ptr = self.inner.context.memory_allocator.map_memory(allocation)?;

                ptr::copy_nonoverlapping(src_ptr, dst_ptr as *mut c_void, copy_len);

                self.inner.context.memory_allocator.unmap_memory(allocation);
            };

            let copy_command_buffer = self.inner.context.device.create_temp_command_bufer(true)?;

            let copy_region = vk::BufferCopy::builder()
                .src_offset(0)
                .dst_offset(dst_offset as u64)
                .size(copy_len as u64);

            unsafe {
                self.inner.context.device.logical_device.cmd_copy_buffer(
                    copy_command_buffer,
                    buffer,
                    self.inner.handle,
                    &[*copy_region],
                );

                self.inner
                    .context
                    .device
                    .logical_device
                    .end_command_buffer(copy_command_buffer)?;

                let command_buffers = [copy_command_buffer];

                let submit_info = vk::SubmitInfo::builder().command_buffers(&command_buffers);

                self.inner.context.device.logical_device.queue_submit(
                    self.inner.context.device.graphics_queue,
                    &[*submit_info],
                    vk::Fence::null(),
                )?;

                self.inner
                    .context
                    .device
                    .logical_device
                    .queue_wait_idle(self.inner.context.device.graphics_queue)?;

                self.inner
                    .context
                    .memory_allocator
                    .destroy_buffer(buffer, allocation);

                self.inner
                    .context
                    .device
                    .destroy_temp_command_buffer(copy_command_buffer);
            }
        }

        Ok(())
    }

    pub fn copy_ptr(&self, ptr: *const c_void, src_size: usize) -> Result<(), VulkanError> {
        self.copy_ptr_with_offset(ptr, src_size, 0)
    }

    pub fn get_handle(&self) -> vk::Buffer {
        self.inner.handle
    }

    pub fn builder() -> BufferBuilder {
        BufferBuilder {
            buffer_size: 0,
            usage_flags: vk::BufferUsageFlags::empty(),
            memory_flags: vk::MemoryPropertyFlags::empty(),
            memory_usage: vk_mem::MemoryUsage::Unknown,
        }
    }
}

impl Drop for VkBuffer {
    fn drop(&mut self) {
        unsafe {
            if self.allocation_info.get_mapped_data() != std::ptr::null_mut() {
                self.context.memory_allocator.unmap_memory(self.allocation);
            }

            self.context
                .memory_allocator
                .destroy_buffer(self.handle, self.allocation);
        }
    }
}

pub struct BufferBuilder {
    buffer_size: u64,
    usage_flags: vk::BufferUsageFlags,
    memory_usage: vk_mem::MemoryUsage,
    memory_flags: vk::MemoryPropertyFlags,
}

#[allow(dead_code)]
impl BufferBuilder {
    pub fn size(mut self, size: u64) -> Self {
        self.buffer_size = size;
        self
    }

    pub fn buffer_usage_flags(mut self, usage_flags: vk::BufferUsageFlags) -> Self {
        self.usage_flags = usage_flags;
        self
    }

    pub fn memory_usage(mut self, memory_usage: vk_mem::MemoryUsage) -> Self {
        self.memory_usage = memory_usage;
        self
    }

    pub fn memory_flags(mut self, memory_flags: vk::MemoryPropertyFlags) -> Self {
        self.memory_flags = memory_flags;
        self
    }

    pub fn build(self, context: Arc<Context>) -> Result<Buffer, VulkanError> {
        Buffer::new(
            context,
            self.buffer_size,
            self.usage_flags,
            self.memory_usage,
            self.memory_flags,
        )
    }
}

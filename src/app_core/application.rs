use crate::{
    app_core::{
        context::Context,
        event::{KeyState, KeyboardEvent},
    },
    vulkan::context::{self, Context as VkContext},
};

use super::{config::Config, event::Event, window::Window};
use anyhow::Result;
use log::*;
use sdl2::{event::Event as SDLEvent, keyboard::Keycode};
use simplelog::*;
use std::{
    collections::HashSet,
    hash::Hash,
    sync::{Arc, Mutex, RwLock},
    time::Instant,
};

pub trait Application {
    fn new(context: Arc<Context>) -> Result<Self>
    where
        Self: Sized;

    fn init(&mut self, context: Arc<Context>) -> Result<()>;
    fn render(&mut self, context: Arc<Context>, delta_time: f32) -> Result<()>;

    // Input, Window, etc events
    fn on_event(&mut self, context: Arc<Context>, event: Event) -> Result<()>;
}

pub fn init_application<App: Application>(config: Config) -> Result<()> {
    TermLogger::init(
        LevelFilter::max(),
        simplelog::Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )?;

    info!("Creating window");
    let window = Window::new(&config)?;

    let mut event_pump = window.get_event_pump()?;

    let vk_context = Arc::new(VkContext::new(&window, &config)?);
    let app_context = Arc::new(Context {
        vulkan_context: vk_context.clone(),
        window,
        pressed_keys: RwLock::new(HashSet::new()),
        pressed_buttons: RwLock::new(HashSet::new()),
    });

    let mut app = App::new(app_context.clone())?;
    app.init(app_context.clone())?;

    let mut frame_count = 0;
    let mut delta_time = 0.0;

    let mut timer = 0;
    let mut frame_time_sum = 0.0;

    'main_loop: loop {
        let start_time = Instant::now();

        for event in event_pump.poll_iter() {
            match event {
                SDLEvent::Quit { .. } => break 'main_loop,

                SDLEvent::KeyDown {
                    timestamp: _,
                    window_id: _,
                    keycode,
                    scancode: _,
                    keymod: _,
                    repeat,
                } => {
                    if let Some(key) = keycode {
                        app.on_event(
                            app_context.clone(),
                            Event::Keyboard(KeyboardEvent {
                                state: KeyState::Down(repeat),
                                keycode: key,
                            }),
                        )?
                    }
                }

                SDLEvent::KeyUp {
                    timestamp: _,
                    window_id: _,
                    keycode,
                    scancode: _,
                    keymod: _,
                    repeat: _,
                } => {
                    if let Some(key) = keycode {
                        app.on_event(
                            app_context.clone(),
                            Event::Keyboard(KeyboardEvent {
                                state: KeyState::Up,
                                keycode: key,
                            }),
                        )?
                    }
                }

                _ => {}
            }
        }

        {
            *app_context.pressed_keys.write().unwrap() = event_pump
                .keyboard_state()
                .pressed_scancodes()
                .filter_map(Keycode::from_scancode)
                .collect();

            *app_context.pressed_buttons.write().unwrap() =
                event_pump.mouse_state().pressed_mouse_buttons().collect();
        }

        app.render(app_context.clone(), delta_time)?;

        frame_count += 1;

        let time_diff = Instant::now() - start_time;
        delta_time = time_diff.as_millis() as f32 * 0.001;

        timer += time_diff.as_millis();
        if timer == 1000 {
            frame_count = 0;
            timer = 0;
        }
    }

    Ok(())
}

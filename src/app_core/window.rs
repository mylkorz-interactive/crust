use super::config::Config;
use anyhow::Result;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum WindowError {
    #[error("Failed to create window: {0}")]
    CreateError(String),
    #[error("Failed to build window")]
    WindowBuildError(sdl2::video::WindowBuildError),
}

pub struct Window {
    pub(crate) sdl_context: sdl2::Sdl,
    pub(crate) _video_subsystem: sdl2::VideoSubsystem,
    pub(crate) window_handle: sdl2::video::Window,
    pub(crate) width: u32,
    pub(crate) height: u32,
}

impl Window {
    pub fn new(config: &Config) -> Result<Window, WindowError> {
        let sdl_context = sdl2::init().map_err(WindowError::CreateError)?;
        let video_subsystem = sdl_context.video().map_err(WindowError::CreateError)?;

        let window_handle = video_subsystem
            .window(
                &config.window_title,
                config.window_width,
                config.window_height,
            )
            .vulkan()
            .position_centered()
            .build()
            .map_err(WindowError::WindowBuildError)?;

        Ok(Window {
            sdl_context,
            _video_subsystem: video_subsystem,
            window_handle,
            width: config.window_width,
            height: config.window_height,
        })
    }

    pub fn get_window_handle(&self) -> &sdl2::video::Window {
        &self.window_handle
    }

    pub fn get_event_pump(&self) -> Result<sdl2::EventPump, WindowError> {
        Ok(self
            .sdl_context
            .event_pump()
            .map_err(WindowError::CreateError)?)
    }

    pub fn get_width(&self) -> u32 {
        self.width
    }

    pub fn get_height(&self) -> u32 {
        self.height
    }
}

use glam::*;

pub struct AABB {
    position: Vec2,
    half_size: Vec2,
}

impl AABB {
    pub fn new(position: Vec2, size: Vec2) -> Self {
        Self {
            position,
            half_size: size / 2.0,
        }
    }

    pub fn overlaps(&self, other: &AABB) -> bool {
        let self_corners = (self.get_top_left(), self.get_bottom_right());
        let other_corners = (other.get_top_left(), other.get_bottom_right());

        if self_corners.0.x < other_corners.1.x
            && self_corners.1.x > other_corners.0.x
            && self_corners.0.y < other_corners.1.y
            && self_corners.1.y > other_corners.0.y
        {
            return true;
        }

        false
    }

    pub fn get_position(&self) -> Vec2 {
        self.position
    }

    pub fn set_position(&mut self, position: Vec2) {
        self.position = position;
    }

    pub fn get_size(&self) -> Vec2 {
        self.half_size * 2.0
    }

    pub fn set_size(&mut self, size: Vec2) {
        self.half_size = size / 2.0
    }

    pub fn get_corners(&self) -> (Vec2, Vec2, Vec2, Vec2) {
        (
            self.get_top_left(),
            self.get_top_right(),
            self.get_bottom_left(),
            self.get_bottom_right(),
        )
    }

    pub fn get_top_left(&self) -> Vec2 {
        let mut top_left = self.position;
        top_left.x -= self.half_size.x;
        top_left.y -= self.half_size.y;

        top_left
    }

    pub fn get_top_right(&self) -> Vec2 {
        let mut top_right = self.position;
        top_right.x += self.half_size.x;
        top_right.y -= self.half_size.y;

        top_right
    }

    pub fn get_bottom_left(&self) -> Vec2 {
        let mut bottom_left = self.position;
        bottom_left.x -= self.half_size.x;
        bottom_left.y += self.half_size.y;

        bottom_left
    }

    pub fn get_bottom_right(&self) -> Vec2 {
        let mut bottom_right = self.position;
        bottom_right.x += self.half_size.x;
        bottom_right.y += self.half_size.y;

        bottom_right
    }
}

use super::{buffer::Buffer, context::Context, texture::Texture, VulkanError};
use align_data::*;
use ash::vk::{
    self, DescriptorBindingFlags, DescriptorPoolCreateFlags, DescriptorSetLayoutCreateFlags,
    DescriptorType, ImageLayout, ShaderStageFlags,
};
use ash::vk::{BufferUsageFlags, CommandBuffer, MemoryPropertyFlags};
use bytemuck::*;
use log::*;
use std::ffi::CString;
use std::{
    mem::size_of,
    sync::{Arc, Mutex},
};

/// (x, y, w, h, u, v, u_extent, v_extent, r, g, b, a, rotation, material_id) -> 14
const SPRITE_ATTRIB_COUNT: usize = 14;

const SPRITE_POSITION_X_ATTRIB_ID: usize = 0;
const SPRITE_POSITION_Y_ATTRIB_ID: usize = 1;
const SPRITE_WIDTH_ATTRIB_ID: usize = 2;
const SPRITE_HEIGHT_ATTRIB_ID: usize = 3;
const SPRITE_UV_U_ATTRIB_ID: usize = 4;
const SPRITE_UV_V_ATTRIB_ID: usize = 5;
const SPRITE_UV_U2_ATTRIB_ID: usize = 6;
const SPRITE_UV_V2_ATTRIB_ID: usize = 7;
const SPRITE_COLOR_R_ATTRIB_ID: usize = 8;
const SPRITE_COLOR_G_ATTRIB_ID: usize = 9;
const SPRITE_COLOR_B_ATTRIB_ID: usize = 10;
const SPRITE_COLOR_A_ATTRIB_ID: usize = 11;
const SPRITE_ROTATION_ATTRIB_ID: usize = 12;

static VERTEX_SHADER: &[u8] = include_aligned!(Align32, "../../shaders/batch.vert.spv");
static FRAGMENT_SHADER: &[u8] = include_aligned!(Align32, "../../shaders/batch.frag.spv");

pub struct Sprite<T: Batchable + Clone> {
    pub(crate) batchable: T,
    pub(crate) instance_data: [f32; SPRITE_ATTRIB_COUNT - 1],
}

impl<T: Batchable + Clone> Sprite<T> {
    pub fn new(batchable: T) -> Self {
        let position = glam::vec2(0.0, 0.0);
        let size = batchable.get_size();
        let color = glam::vec4(1.0, 1.0, 1.0, 1.0);
        let rotation = 0.0;

        let mut sprite = Self {
            batchable: batchable.clone(),
            instance_data: [0.0; SPRITE_ATTRIB_COUNT - 1],
        };

        sprite.set_batchable(batchable);
        sprite.set_position(position);
        sprite.set_size(glam::vec2(size.0 as f32, size.1 as f32));
        sprite.set_color(color);
        sprite.set_rotation(rotation);

        sprite
    }

    pub fn set_position(&mut self, position: glam::Vec2) {
        self.instance_data[SPRITE_POSITION_X_ATTRIB_ID] = position.x;
        self.instance_data[SPRITE_POSITION_Y_ATTRIB_ID] = position.y;
    }

    pub fn get_position(&self) -> glam::Vec2 {
        glam::Vec2::from_slice(&self.instance_data[SPRITE_POSITION_X_ATTRIB_ID..])
    }

    pub fn set_size(&mut self, size: glam::Vec2) {
        self.instance_data[SPRITE_WIDTH_ATTRIB_ID] = size.x;
        self.instance_data[SPRITE_HEIGHT_ATTRIB_ID] = size.y;
    }

    pub fn get_size(&self) -> glam::Vec2 {
        glam::Vec2::from_slice(&self.instance_data[SPRITE_WIDTH_ATTRIB_ID..])
    }

    pub fn set_color(&mut self, color: glam::Vec4) {
        self.instance_data[SPRITE_COLOR_R_ATTRIB_ID] = color.x;
        self.instance_data[SPRITE_COLOR_G_ATTRIB_ID] = color.y;
        self.instance_data[SPRITE_COLOR_B_ATTRIB_ID] = color.z;
        self.instance_data[SPRITE_COLOR_A_ATTRIB_ID] = color.w;
    }

    pub fn get_color(&self) -> glam::Vec4 {
        glam::Vec4::from_slice(&self.instance_data[SPRITE_COLOR_R_ATTRIB_ID..])
    }

    pub fn set_rotation(&mut self, rotation: f32) {
        self.instance_data[SPRITE_ROTATION_ATTRIB_ID] = rotation;
    }

    pub fn get_rotation(&self) -> f32 {
        self.instance_data[SPRITE_ROTATION_ATTRIB_ID]
    }

    pub fn set_batchable(&mut self, batchable: T) {
        let (u, v, u2, v2) = batchable.get_uvs();

        self.batchable = batchable;

        self.instance_data[SPRITE_UV_U_ATTRIB_ID] = u;
        self.instance_data[SPRITE_UV_V_ATTRIB_ID] = v;
        self.instance_data[SPRITE_UV_U2_ATTRIB_ID] = u2;
        self.instance_data[SPRITE_UV_V2_ATTRIB_ID] = v2;
    }

    pub fn get_batchable(&self) -> T {
        self.batchable.clone()
    }
}

pub trait Batchable {
    /// Get the texture handle of the object
    fn get_texture(&self) -> Texture;

    /// Get uvs for the indices (u, v, u_extent, v_extent)
    fn get_uvs(&self) -> (f32, f32, f32, f32);

    /// Get the size of the item
    fn get_size(&self) -> (u32, u32);
}

impl Batchable for Texture {
    fn get_texture(&self) -> Texture {
        self.clone()
    }

    fn get_uvs(&self) -> (f32, f32, f32, f32) {
        (0.0, 0.0, 1.0, 1.0)
    }

    fn get_size(&self) -> (u32, u32) {
        let dims = self.get_texture().get_dimensions();

        (dims.0, dims.1)
    }
}

/// Simple quad-based batcher
/// TODO: error handling and tracking whether the batch is currently drawing
pub struct SpriteBatch {
    /// How many sprites the batch fits before flushing
    capacity: usize,

    /// How many sprites are being rendered currently
    sprite_count: usize,

    /// Pointer to the vertex buffer data, we would like to have this buffer be mappable with coherent access
    /// TODO: what if we don't get coherent access?
    vtx: *mut f32,

    context: Arc<Context>,

    vertex_buffer: Buffer,
    uniform_buffer: Buffer,

    /// Is the batch currently collecting draw calls
    active: bool,

    descriptor_pool: vk::DescriptorPool,
    descriptor_set_layout: vk::DescriptorSetLayout,
    descriptor_set: vk::DescriptorSet,

    vertex_shader_module: vk::ShaderModule,
    fragment_shader_module: vk::ShaderModule,

    default_render_pass: vk::RenderPass,

    default_pipeline: vk::Pipeline,
    default_pipeline_layout: vk::PipelineLayout,

    // Textures that were used during one `begin()`/`end()` cycle
    used_textures: Vec<Texture>,
}

unsafe impl Send for SpriteBatch {}
unsafe impl Sync for SpriteBatch {}

impl SpriteBatch {
    pub fn new(capacity: usize, vk_context: Arc<Context>) -> Result<Arc<Mutex<Self>>, VulkanError> {
        // Create the needed buffers
        let vertex_buffer_size = SPRITE_ATTRIB_COUNT * capacity * size_of::<f32>();
        let vertex_buffer = Buffer::builder()
            .size(vertex_buffer_size as u64)
            .buffer_usage_flags(BufferUsageFlags::VERTEX_BUFFER)
            .memory_usage(vk_mem::MemoryUsage::CpuToGpu)
            .memory_flags(MemoryPropertyFlags::HOST_COHERENT | MemoryPropertyFlags::HOST_VISIBLE)
            .build(vk_context.clone())?;

        let uniform_buffer = Buffer::builder()
            .size(size_of::<glam::Mat4>() as u64)
            .buffer_usage_flags(BufferUsageFlags::UNIFORM_BUFFER)
            .memory_usage(vk_mem::MemoryUsage::CpuToGpu)
            .memory_flags(MemoryPropertyFlags::HOST_COHERENT | MemoryPropertyFlags::HOST_VISIBLE)
            .build(vk_context.clone())?;

        // The absolutely horrible process of pipeline creation
        // Shader stage
        let vertex_shader_module_create_info = vk::ShaderModuleCreateInfo::builder()
            .code(try_cast_slice(VERTEX_SHADER).expect("Error casting vertex shader bytes"));

        let vertex_shader_module = unsafe {
            vk_context
                .device
                .logical_device
                .create_shader_module(&vertex_shader_module_create_info, None)?
        };

        let fragment_shader_module_create_info = vk::ShaderModuleCreateInfo::builder()
            .code(try_cast_slice(FRAGMENT_SHADER).expect("Error casting fragment shader bytes"));

        let fragment_shader_module = unsafe {
            vk_context
                .device
                .logical_device
                .create_shader_module(&fragment_shader_module_create_info, None)?
        };

        let shader_entry_point = CString::new("main").expect("Failed to create string");

        let pipeline_vertex_shader_stage = vk::PipelineShaderStageCreateInfo::builder()
            .stage(vk::ShaderStageFlags::VERTEX)
            .name(&shader_entry_point)
            .module(vertex_shader_module)
            .build();

        let pipeline_fragment_shader_stage = vk::PipelineShaderStageCreateInfo::builder()
            .stage(vk::ShaderStageFlags::FRAGMENT)
            .name(&shader_entry_point)
            .module(fragment_shader_module)
            .build();

        let shader_stages = [pipeline_vertex_shader_stage, pipeline_fragment_shader_stage];

        // Input assembly stage
        let vertex_input_description = vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride((SPRITE_ATTRIB_COUNT * size_of::<f32>()) as u32)
            .input_rate(vk::VertexInputRate::INSTANCE)
            .build();

        let vertex_input_descriptions = [vertex_input_description];

        // (x, y, w, h, u, v, u_extent, v_extent, r, g, b, a, rotation, material_id) -> 14
        let position_size_description = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(0)
            .format(vk::Format::R32G32B32A32_SFLOAT)
            .offset(0)
            .build();

        let uv_description = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(1)
            .format(vk::Format::R32G32B32A32_SFLOAT)
            .offset((4 * size_of::<f32>()) as u32)
            .build();

        let color_description = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(2)
            .format(vk::Format::R32G32B32A32_SFLOAT)
            .offset((8 * size_of::<f32>()) as u32)
            .build();

        let material_rotation_description = vk::VertexInputAttributeDescription::builder()
            .binding(0)
            .location(3)
            .format(vk::Format::R32G32_SFLOAT)
            .offset((12 * size_of::<f32>()) as u32)
            .build();

        let vertex_attribute_descriptions = [
            position_size_description,
            uv_description,
            color_description,
            material_rotation_description,
        ];

        let pipeline_vertex_input_state = vk::PipelineVertexInputStateCreateInfo::builder()
            .vertex_binding_descriptions(&vertex_input_descriptions)
            .vertex_attribute_descriptions(&vertex_attribute_descriptions);

        let pipeline_input_assembly_state = vk::PipelineInputAssemblyStateCreateInfo::builder()
            .topology(vk::PrimitiveTopology::TRIANGLE_LIST)
            .primitive_restart_enable(false);

        // Viewport state
        let viewports = [vk::Viewport::default()];
        let scissor_regions = [vk::Rect2D::default()];

        let pipeline_viewport_state = vk::PipelineViewportStateCreateInfo::builder()
            .viewports(&viewports)
            .scissors(&scissor_regions);

        // Rasterization state
        let pipeline_rasterizer_state = vk::PipelineRasterizationStateCreateInfo::builder()
            .depth_clamp_enable(false)
            .rasterizer_discard_enable(false)
            .polygon_mode(vk::PolygonMode::FILL)
            .line_width(1.0)
            .cull_mode(vk::CullModeFlags::BACK)
            .front_face(vk::FrontFace::CLOCKWISE)
            .depth_bias_enable(false)
            .depth_bias_constant_factor(0.0)
            .depth_bias_clamp(0.0)
            .depth_bias_slope_factor(0.0);

        // Multisample state
        let pipeline_multisample_state = vk::PipelineMultisampleStateCreateInfo::builder()
            .sample_shading_enable(false)
            .rasterization_samples(vk::SampleCountFlags::TYPE_1)
            .min_sample_shading(1.0)
            .sample_mask(&[])
            .alpha_to_coverage_enable(false)
            .alpha_to_one_enable(false);

        // Color blend state
        let pipeline_color_attachment_blend_state =
            vk::PipelineColorBlendAttachmentState::builder()
                .color_write_mask(
                    vk::ColorComponentFlags::R
                        | vk::ColorComponentFlags::G
                        | vk::ColorComponentFlags::B
                        | vk::ColorComponentFlags::A,
                )
                .blend_enable(true)
                .src_color_blend_factor(vk::BlendFactor::SRC_ALPHA)
                .dst_color_blend_factor(vk::BlendFactor::ONE_MINUS_SRC_ALPHA)
                .color_blend_op(vk::BlendOp::ADD)
                .src_alpha_blend_factor(vk::BlendFactor::ONE)
                .dst_alpha_blend_factor(vk::BlendFactor::ZERO)
                .alpha_blend_op(vk::BlendOp::ADD)
                .build();

        let color_attachments = [pipeline_color_attachment_blend_state];

        let pipeline_color_blend_state = vk::PipelineColorBlendStateCreateInfo::builder()
            .logic_op_enable(false)
            .logic_op(vk::LogicOp::COPY)
            .attachments(&color_attachments)
            .blend_constants([0.0, 0.0, 0.0, 0.0]);

        // Dynamic state
        let pipeline_dynamic_state = vk::PipelineDynamicStateCreateInfo::builder()
            .dynamic_states(&[vk::DynamicState::VIEWPORT, vk::DynamicState::SCISSOR]);

        // Descriptor state
        // Descriptor pool
        // TODO: no hardcoding
        let descriptor_pool_sizes = [
            vk::DescriptorPoolSize::builder()
                .descriptor_count(1024)
                .ty(DescriptorType::COMBINED_IMAGE_SAMPLER)
                .build(),
            vk::DescriptorPoolSize::builder()
                .descriptor_count(1)
                .ty(DescriptorType::UNIFORM_BUFFER)
                .build(),
        ];

        let descriptor_pool_info = vk::DescriptorPoolCreateInfo::builder()
            .flags(DescriptorPoolCreateFlags::UPDATE_AFTER_BIND)
            .pool_sizes(&descriptor_pool_sizes)
            .max_sets(1024);

        let descriptor_pool = unsafe {
            vk_context
                .device
                .logical_device
                .create_descriptor_pool(&descriptor_pool_info, None)?
        };

        // Descriptor set
        // TODO: hardcode
        let descriptor_layout_bindings = [
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(DescriptorType::COMBINED_IMAGE_SAMPLER)
                .descriptor_count(1024)
                .binding(1)
                .stage_flags(ShaderStageFlags::ALL)
                .build(),
            vk::DescriptorSetLayoutBinding::builder()
                .descriptor_type(DescriptorType::UNIFORM_BUFFER)
                .descriptor_count(1)
                .binding(0)
                .stage_flags(ShaderStageFlags::ALL)
                .build(),
        ];

        let mut layout_binding_flags_info =
            vk::DescriptorSetLayoutBindingFlagsCreateInfo::builder()
                .binding_flags(&[
                    DescriptorBindingFlags::PARTIALLY_BOUND
                        | DescriptorBindingFlags::VARIABLE_DESCRIPTOR_COUNT
                        | DescriptorBindingFlags::UPDATE_AFTER_BIND,
                    DescriptorBindingFlags::UPDATE_AFTER_BIND,
                ])
                .build();

        let layout_create_info = vk::DescriptorSetLayoutCreateInfo::builder()
            .bindings(&descriptor_layout_bindings)
            .flags(DescriptorSetLayoutCreateFlags::UPDATE_AFTER_BIND_POOL)
            .push_next(&mut layout_binding_flags_info);

        let descriptor_set_layout = unsafe {
            vk_context
                .device
                .logical_device
                .create_descriptor_set_layout(&layout_create_info, None)?
        };

        let descriptor_layouts = [descriptor_set_layout];

        let mut variable_descriptor_count_allocate_info =
            vk::DescriptorSetVariableDescriptorCountAllocateInfo::builder()
                .descriptor_counts(&[1023]);

        let descriptor_set_allocate_info = vk::DescriptorSetAllocateInfo::builder()
            .descriptor_pool(descriptor_pool)
            .set_layouts(&descriptor_layouts)
            .push_next(&mut variable_descriptor_count_allocate_info);

        let descriptor_sets = unsafe {
            vk_context
                .device
                .logical_device
                .allocate_descriptor_sets(&descriptor_set_allocate_info)?
        };

        let pipeline_layout_create_info = vk::PipelineLayoutCreateInfo::builder()
            .set_layouts(&[descriptor_set_layout])
            .build();

        let pipeline_layout = unsafe {
            vk_context
                .device
                .logical_device
                .create_pipeline_layout(&pipeline_layout_create_info, None)?
        };

        let color_attachment = vk::AttachmentDescription::builder()
            .format(vk_context.swapchain.surface_format.format)
            .load_op(vk::AttachmentLoadOp::CLEAR)
            .store_op(vk::AttachmentStoreOp::STORE)
            .stencil_load_op(vk::AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(vk::AttachmentStoreOp::DONT_CARE)
            .initial_layout(vk::ImageLayout::UNDEFINED)
            .final_layout(vk::ImageLayout::PRESENT_SRC_KHR)
            .samples(vk::SampleCountFlags::TYPE_1)
            .build();

        let attachment_reference = vk::AttachmentReference::builder()
            .layout(vk::ImageLayout::COLOR_ATTACHMENT_OPTIMAL)
            .build();

        let subpass = vk::SubpassDescription::builder()
            .pipeline_bind_point(vk::PipelineBindPoint::GRAPHICS)
            .color_attachments(&[attachment_reference])
            .build();

        let subpass_dependancy = vk::SubpassDependency::builder()
            .src_subpass(vk::SUBPASS_EXTERNAL)
            .dst_subpass(0)
            .src_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .src_access_mask(vk::AccessFlags::empty())
            .dst_stage_mask(vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
            .dst_access_mask(vk::AccessFlags::COLOR_ATTACHMENT_WRITE)
            .build();

        let subpass_dependancies = vec![subpass_dependancy];

        let attachments = [color_attachment];
        let subpasses = [subpass];

        let render_pass_create_info = vk::RenderPassCreateInfo::builder()
            .attachments(&attachments)
            .subpasses(&subpasses)
            .dependencies(&subpass_dependancies);

        let render_pass = unsafe {
            vk_context
                .device
                .logical_device
                .create_render_pass(&render_pass_create_info, None)?
        };

        let pipeline_create_info = vk::GraphicsPipelineCreateInfo::builder()
            .stages(&shader_stages)
            .vertex_input_state(&pipeline_vertex_input_state)
            .input_assembly_state(&pipeline_input_assembly_state)
            .viewport_state(&pipeline_viewport_state)
            .rasterization_state(&pipeline_rasterizer_state)
            .multisample_state(&pipeline_multisample_state)
            .color_blend_state(&pipeline_color_blend_state)
            .layout(pipeline_layout)
            .render_pass(render_pass)
            .subpass(0)
            .dynamic_state(&pipeline_dynamic_state)
            .build();

        let pipelines = unsafe {
            vk_context
                .device
                .logical_device
                .create_graphics_pipelines(vk::PipelineCache::null(), &[pipeline_create_info], None)
                .unwrap()
        };

        let buffer_info = vk::DescriptorBufferInfo::builder()
            .buffer(uniform_buffer.get_handle())
            .offset(0)
            .range(size_of::<glam::Mat4>() as u64)
            .build();

        let buffer_infos = [buffer_info];

        let write_set = vk::WriteDescriptorSet::builder()
            .descriptor_type(DescriptorType::UNIFORM_BUFFER)
            .dst_set(descriptor_sets[0])
            .dst_binding(0)
            .dst_array_element(0)
            .buffer_info(&buffer_infos)
            .build();

        unsafe {
            vk_context
                .device
                .logical_device
                .update_descriptor_sets(&[write_set], &[]);
        }

        let batch = SpriteBatch {
            capacity: capacity,
            sprite_count: 0,
            vtx: std::ptr::null_mut(),
            context: vk_context.clone(),
            vertex_buffer,
            uniform_buffer,
            active: false,
            descriptor_pool,
            descriptor_set: descriptor_sets[0],
            descriptor_set_layout,
            default_pipeline_layout: pipeline_layout,
            default_pipeline: pipelines[0],
            vertex_shader_module,
            fragment_shader_module,
            default_render_pass: render_pass,
            used_textures: Vec::new(),
        };

        Ok(Arc::new(Mutex::new(batch)))
    }

    /// Begin recording render commands
    pub fn begin(&mut self, combined_matrix: glam::Mat4) {
        self.sprite_count = 0;

        self.uniform_buffer
            .copy_data(bytemuck::bytes_of(&combined_matrix))
            .unwrap();

        // TODO: u n w r a p
        self.vtx = self.vertex_buffer.map_memory().unwrap() as *mut f32;

        self.used_textures.clear();

        self.active = true;
    }

    /// End batch, will trigger a flush
    pub fn end(&mut self, command_buffer: CommandBuffer) {
        self.vertex_buffer.unmap_memory();

        self.flush(command_buffer);

        self.active = false;
    }

    /// Flush the batch, inserting the commands in the command buffer, must be called between `begin()` and `end()` calls
    pub fn flush(&mut self, command_buffer: CommandBuffer) {
        if self.sprite_count == 0 {
            return;
        }

        unsafe {
            self.context.device.logical_device.cmd_bind_pipeline(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.default_pipeline,
            );
        }

        let mut idx = 0;
        for texture in &self.used_textures {
            let image_info = vk::DescriptorImageInfo::builder()
                .sampler(texture.get_sampler())
                .image_view(texture.get_image_view())
                .image_layout(ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                .build();

            let image_infos = [image_info];

            let write_set = vk::WriteDescriptorSet::builder()
                .descriptor_type(DescriptorType::COMBINED_IMAGE_SAMPLER)
                .dst_set(self.descriptor_set)
                .dst_binding(1)
                .dst_array_element(idx)
                .image_info(&image_infos)
                .build();

            unsafe {
                self.context
                    .device
                    .logical_device
                    .update_descriptor_sets(&[write_set], &[]);
            }

            idx += 1;
        }

        unsafe {
            self.context.device.logical_device.cmd_bind_descriptor_sets(
                command_buffer,
                vk::PipelineBindPoint::GRAPHICS,
                self.default_pipeline_layout,
                0,
                &[self.descriptor_set],
                &[],
            );

            let vertex_buffers = [self.vertex_buffer.get_handle()];
            let offsets = [0];

            self.context.device.logical_device.cmd_bind_vertex_buffers(
                command_buffer,
                0,
                &vertex_buffers,
                &offsets,
            );

            self.context.device.logical_device.cmd_draw(
                command_buffer,
                6,
                self.sprite_count as u32,
                0,
                0,
            );
        }
    }

    pub fn draw_sprite<T: Batchable + Clone>(&mut self, sprite: &Sprite<T>) {
        if self.sprite_count == self.capacity {
            warn!("Exeeded sprite batch capacity, render call will be ignored");
            return;
        }

        let texture_id = self
            .used_textures
            .iter()
            .position(|tex| tex.get_handle() == sprite.batchable.get_texture().get_handle());

        let material_index;

        if let Some(texture_id) = texture_id {
            material_index = texture_id;
        } else {
            material_index = self.used_textures.len();
            self.used_textures.push(sprite.batchable.get_texture());
        }

        unsafe {
            std::ptr::copy_nonoverlapping(
                sprite.instance_data.as_ptr(),
                self.vtx
                    .offset((self.sprite_count * SPRITE_ATTRIB_COUNT) as isize),
                SPRITE_ATTRIB_COUNT - 1,
            );

            *self.vtx.offset(
                (self.sprite_count * SPRITE_ATTRIB_COUNT + SPRITE_ATTRIB_COUNT - 1) as isize,
            ) = material_index as f32;
        }

        self.sprite_count += 1;
    }

    pub fn draw<T: Batchable + Clone>(
        &mut self,
        batchable: &T,
        x: f32,
        y: f32,
        w: f32,
        h: f32,
        rotation: Option<f32>,
        color: Option<(f32, f32, f32, f32)>,
    ) {
        if self.sprite_count == self.capacity {
            warn!("Exeeded sprite batch capacity, render call will be ignored");
            return;
        }

        let texture_id = self
            .used_textures
            .iter()
            .position(|tex| tex.get_handle() == batchable.get_texture().get_handle());

        let material_index;

        if let Some(texture_id) = texture_id {
            material_index = texture_id;
        } else {
            material_index = self.used_textures.len();
            self.used_textures.push(batchable.get_texture());
        }

        let vtx = unsafe {
            std::slice::from_raw_parts_mut(self.vtx, self.capacity * SPRITE_ATTRIB_COUNT)
        };
        let offset = self.sprite_count * SPRITE_ATTRIB_COUNT;

        let uvs = batchable.get_uvs();
        let rot = if let Some(rotation) = rotation {
            rotation
        } else {
            0.0
        };
        let col = if let Some(color) = color {
            color
        } else {
            (1.0, 1.0, 1.0, 1.0)
        };

        vtx[offset] = x;
        vtx[offset + 1] = y;
        vtx[offset + 2] = w;
        vtx[offset + 3] = h;
        vtx[offset + 4] = uvs.0;
        vtx[offset + 5] = uvs.1;
        vtx[offset + 6] = uvs.2;
        vtx[offset + 7] = uvs.3;
        vtx[offset + 8] = col.0;
        vtx[offset + 9] = col.1;
        vtx[offset + 10] = col.2;
        vtx[offset + 11] = col.3;
        vtx[offset + 12] = rot;
        vtx[offset + 12] = material_index as f32;

        self.sprite_count += 1;
    }

    pub fn get_render_pass(&self) -> vk::RenderPass {
        self.default_render_pass
    }

    pub fn get_capacity(&self) -> usize {
        self.capacity
    }

    pub fn get_sprite_count(&self) -> usize {
        self.sprite_count
    }
}

impl Drop for SpriteBatch {
    fn drop(&mut self) {
        // TODO: The dreaded cleanup of resources
        unsafe {
            self.context
                .device
                .logical_device
                .destroy_pipeline_layout(self.default_pipeline_layout, None);

            self.context
                .device
                .logical_device
                .destroy_pipeline(self.default_pipeline, None);

            self.context
                .device
                .logical_device
                .destroy_descriptor_set_layout(self.descriptor_set_layout, None);

            self.context
                .device
                .logical_device
                .destroy_descriptor_pool(self.descriptor_pool, None);

            self.context
                .device
                .logical_device
                .destroy_render_pass(self.default_render_pass, None);

            self.context
                .device
                .logical_device
                .destroy_shader_module(self.vertex_shader_module, None);

            self.context
                .device
                .logical_device
                .destroy_shader_module(self.fragment_shader_module, None);
        }
    }
}

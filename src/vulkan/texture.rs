use crate::vulkan::{context::Context, VulkanError};
use ash::{
    vk::{
        AccessFlags, BufferUsageFlags, DependencyFlags, Extent3D, ImageAspectFlags,
        ImageCreateInfo, ImageLayout, ImageTiling, ImageType, ImageUsageFlags, MemoryPropertyFlags,
        PipelineStageFlags, SampleCountFlags, SharingMode,
    },
    *,
};
use image::{io::Reader as ImageReader, GenericImageView};
use std::sync::Arc;
use vk_mem::*;

use super::buffer::Buffer;

pub(crate) struct VkTexture {
    context: Arc<Context>,

    handle: ash::vk::Image,

    allocation: Allocation,
    allocation_info: AllocationInfo,

    dimensions: (u32, u32, u32),
    layer_count: u32,
    mip_levels: u32,

    sampler: vk::Sampler,
    view: vk::ImageView,
}

unsafe impl Send for VkTexture {}
unsafe impl Sync for VkTexture {}

impl Drop for VkTexture {
    fn drop(&mut self) {
        unsafe {
            self.context
                .memory_allocator
                .destroy_image(self.handle, self.allocation);
        }
    }
}

impl PartialEq for VkTexture {
    fn eq(&self, other: &Self) -> bool {
        self.handle == other.handle
    }
}

#[derive(Clone)]
pub struct Texture {
    pub(crate) inner: Arc<VkTexture>,
}

impl Texture {
    // TODO: supports only simple RGBA8 2D image loading from file, should expand later
    pub fn new_from_file(context: Arc<Context>, path: &str) -> Result<Self, VulkanError> {
        let vk = context;

        // TODO: move image loading out of the texture class, implement proper error handling
        let loaded_image = ImageReader::open(path).unwrap().decode().unwrap();
        let image_dims = loaded_image.dimensions();

        let image_create_info = ImageCreateInfo::builder()
            .image_type(ImageType::TYPE_2D)
            .format(vk::Format::R8G8B8A8_UNORM)
            .extent(Extent3D {
                width: image_dims.0,
                height: image_dims.1,
                depth: 1,
            })
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCountFlags::TYPE_1)
            .tiling(ImageTiling::OPTIMAL)
            .usage(ImageUsageFlags::TRANSFER_DST | ImageUsageFlags::SAMPLED)
            .sharing_mode(SharingMode::EXCLUSIVE)
            .initial_layout(ImageLayout::UNDEFINED);

        let (image, allocation, allocation_info) = unsafe {
            let allocation_info = AllocationCreateInfo {
                usage: MemoryUsage::GpuOnly,
                flags: AllocationCreateFlags::NONE,
                required_flags: MemoryPropertyFlags::empty(),
                preferred_flags: MemoryPropertyFlags::empty(),
                memory_type_bits: 0,
                pool: None,
                user_data: None,
            };

            vk.memory_allocator
                .create_image(&image_create_info, &allocation_info)?
        };

        let image_buffer = loaded_image.into_rgba8();

        let staging_buffer = Buffer::builder()
            .buffer_usage_flags(BufferUsageFlags::TRANSFER_SRC)
            .memory_flags(MemoryPropertyFlags::HOST_VISIBLE)
            .memory_usage(MemoryUsage::CpuOnly)
            .size(image_buffer.as_raw().len() as u64)
            .build(vk.clone())?;

        staging_buffer.copy_data(image_buffer.as_raw())?;

        let copy_command_buffer = vk.device.create_temp_command_bufer(true)?;

        let region = vk::BufferImageCopy::builder()
            .buffer_offset(0)
            .buffer_row_length(image_dims.0)
            .buffer_image_height(image_dims.1)
            .image_subresource(vk::ImageSubresourceLayers {
                aspect_mask: ImageAspectFlags::COLOR,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: 1,
            })
            .image_offset(vk::Offset3D { x: 0, y: 0, z: 0 })
            .image_extent(Extent3D {
                width: image_dims.0,
                height: image_dims.1,
                depth: 1,
            })
            .build();

        let mut image_barrier = vk::ImageMemoryBarrier::builder()
            .old_layout(ImageLayout::UNDEFINED)
            .new_layout(ImageLayout::TRANSFER_DST_OPTIMAL)
            .src_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .dst_queue_family_index(vk::QUEUE_FAMILY_IGNORED)
            .image(image)
            .subresource_range(vk::ImageSubresourceRange {
                aspect_mask: ImageAspectFlags::COLOR,
                level_count: 1,
                layer_count: 1,
                ..Default::default()
            })
            .src_access_mask(AccessFlags::empty())
            .dst_access_mask(AccessFlags::TRANSFER_WRITE)
            .build();

        unsafe {
            vk.device.logical_device.cmd_pipeline_barrier(
                copy_command_buffer,
                PipelineStageFlags::BOTTOM_OF_PIPE,
                PipelineStageFlags::TRANSFER,
                DependencyFlags::empty(),
                &[],
                &[],
                &[image_barrier],
            );

            vk.device.logical_device.cmd_copy_buffer_to_image(
                copy_command_buffer,
                staging_buffer.get_handle(),
                image,
                ImageLayout::TRANSFER_DST_OPTIMAL,
                &[region],
            );

            image_barrier.old_layout = ImageLayout::TRANSFER_DST_OPTIMAL;
            image_barrier.new_layout = ImageLayout::SHADER_READ_ONLY_OPTIMAL;

            vk.device.logical_device.cmd_pipeline_barrier(
                copy_command_buffer,
                PipelineStageFlags::BOTTOM_OF_PIPE,
                PipelineStageFlags::TRANSFER,
                DependencyFlags::empty(),
                &[],
                &[],
                &[image_barrier],
            );

            vk.device
                .logical_device
                .end_command_buffer(copy_command_buffer)?;

            let command_buffers = [copy_command_buffer];

            let submit_info = vk::SubmitInfo::builder().command_buffers(&command_buffers);

            vk.device.logical_device.queue_submit(
                vk.device.graphics_queue,
                &[*submit_info],
                vk::Fence::null(),
            )?;

            vk.device
                .logical_device
                .queue_wait_idle(vk.device.graphics_queue)?;
        }

        let image_view_info = vk::ImageViewCreateInfo::builder()
            .image(image)
            .view_type(vk::ImageViewType::TYPE_2D)
            .format(vk::Format::R8G8B8A8_UNORM)
            .subresource_range(vk::ImageSubresourceRange {
                aspect_mask: vk::ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            });

        let image_view = unsafe {
            vk.device
                .logical_device
                .create_image_view(&image_view_info, None)?
        };

        let sampler_info = vk::SamplerCreateInfo::builder()
            .mag_filter(vk::Filter::NEAREST)
            .min_filter(vk::Filter::NEAREST)
            .address_mode_u(vk::SamplerAddressMode::CLAMP_TO_EDGE)
            .address_mode_v(vk::SamplerAddressMode::CLAMP_TO_EDGE)
            .address_mode_w(vk::SamplerAddressMode::CLAMP_TO_EDGE)
            .anisotropy_enable(false)
            .max_anisotropy(0.0)
            .border_color(vk::BorderColor::INT_OPAQUE_BLACK)
            .unnormalized_coordinates(false)
            .compare_op(vk::CompareOp::ALWAYS)
            .mipmap_mode(vk::SamplerMipmapMode::LINEAR)
            .mip_lod_bias(0.0)
            .min_lod(0.0)
            .max_lod(0.0);

        let sampler = unsafe {
            vk.device
                .logical_device
                .create_sampler(&sampler_info, None)?
        };

        let texture = VkTexture {
            context: vk,
            handle: image,
            allocation,
            allocation_info,
            dimensions: (image_dims.0, image_dims.1, 1),
            layer_count: 1,
            mip_levels: 1,
            view: image_view,
            sampler,
        };

        Ok(Texture {
            inner: Arc::new(texture),
        })
    }

    pub fn get_handle(&self) -> vk::Image {
        self.inner.handle
    }

    pub fn get_sampler(&self) -> vk::Sampler {
        self.inner.sampler
    }

    pub fn get_image_view(&self) -> vk::ImageView {
        self.inner.view
    }

    pub fn get_dimensions(&self) -> (u32, u32, u32) {
        self.inner.dimensions
    }
}

impl PartialEq for Texture {
    fn eq(&self, other: &Self) -> bool {
        self.inner == other.inner
    }
}

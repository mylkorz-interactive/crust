use crate::vulkan::VulkanError;
use ash::vk;
use log::*;

pub struct Device {
    pub logical_device: ash::Device,
    pub physical_device: vk::PhysicalDevice,
    pub graphics_queue_family_index: u32,
    pub graphics_queue: vk::Queue,
    pub memory_properties: vk::PhysicalDeviceMemoryProperties,
    pub features: vk::PhysicalDeviceFeatures,
    pub indexing_features: vk::PhysicalDeviceDescriptorIndexingFeatures,

    // Command pool to allocate one time use command buffers from
    temp_command_pool: vk::CommandPool,
}

impl Device {
    pub fn new(
        instance: &ash::Instance,
        physical_device_handle: vk::PhysicalDevice,
        surface_loader: &ash::extensions::khr::Surface,
        surface: &vk::SurfaceKHR,
        validation_layer_names: &[*const i8],
    ) -> Result<Self, VulkanError> {
        info!("Creating physical device");
        let queue_family_properties =
            unsafe { instance.get_physical_device_queue_family_properties(physical_device_handle) };

        info!("Creating device");
        let (graphics_queue_family_index, _) = queue_family_properties
            .into_iter()
            .enumerate()
            .find(|(index, family)| {
                family.queue_flags.contains(vk::QueueFlags::GRAPHICS)
                    && unsafe {
                        surface_loader.get_physical_device_surface_support(
                            physical_device_handle,
                            *index as u32,
                            *surface,
                        )
                    }
                    .expect("")
            })
            .expect("Could not find a queue that support graphics operations");

        let graphics_queue_family_index = graphics_queue_family_index as u32;

        let device_extensions = vec![ash::extensions::khr::Swapchain::name().as_ptr()];

        let graphics_queue_create_info = vk::DeviceQueueCreateInfo::builder()
            .queue_family_index(graphics_queue_family_index)
            .queue_priorities(&[1.0])
            .build();

        let features = unsafe { instance.get_physical_device_features(physical_device_handle) };

        let mut indexing_features = vk::PhysicalDeviceDescriptorIndexingFeatures::default();

        let mut features_2 =
            vk::PhysicalDeviceFeatures2::builder().push_next(&mut indexing_features);

        unsafe { instance.get_physical_device_features2(physical_device_handle, &mut features_2) };

        let device_create_info = vk::DeviceCreateInfo::builder()
            .queue_create_infos(&[graphics_queue_create_info])
            .enabled_layer_names(&validation_layer_names)
            .enabled_extension_names(&device_extensions)
            .push_next(&mut features_2)
            .build();

        let device = unsafe {
            instance
                .create_device(physical_device_handle, &device_create_info, None)
                .map_err(|e| VulkanError::DeviceError(e.to_string()))?
        };

        let graphics_queue = unsafe { device.get_device_queue(graphics_queue_family_index, 0) };

        let memory_properties =
            unsafe { instance.get_physical_device_memory_properties(physical_device_handle) };

        let command_pool_create_info = vk::CommandPoolCreateInfo::builder()
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(graphics_queue_family_index);

        let temp_command_pool =
            unsafe { device.create_command_pool(&command_pool_create_info, None)? };

        Ok(Device {
            physical_device: physical_device_handle,
            logical_device: device,
            graphics_queue_family_index,
            graphics_queue,
            memory_properties,
            temp_command_pool,
            features,
            indexing_features,
        })
    }

    pub fn create_temp_command_bufer(
        &self,
        auto_begin: bool,
    ) -> Result<vk::CommandBuffer, VulkanError> {
        let command_buffer_allocate_info = vk::CommandBufferAllocateInfo::builder()
            .command_pool(self.temp_command_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let command_buffer = unsafe {
            self.logical_device
                .allocate_command_buffers(&command_buffer_allocate_info)?[0]
        };

        if auto_begin {
            let command_buffer_begin_info = vk::CommandBufferBeginInfo::default();

            unsafe {
                self.logical_device
                    .begin_command_buffer(command_buffer, &command_buffer_begin_info)?;
            }
        }

        Ok(command_buffer)
    }

    pub fn destroy_temp_command_buffer(&self, command_buffer: vk::CommandBuffer) {
        unsafe {
            self.logical_device
                .free_command_buffers(self.temp_command_pool, &[command_buffer]);
        }
    }
}

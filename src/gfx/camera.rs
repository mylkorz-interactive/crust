use glam::*;

pub struct OrthoCamera {
    position: Vec3, //x, y, (z)oom
    viewport: Vec4, // w, h, near, far

    view_matrix: Mat4,
    projection_matrix: Mat4,
    combined_matrix: Mat4,
}

impl OrthoCamera {
    pub fn new(viewport_width: f32, viewport_height: f32) -> Self {
        Self {
            position: vec3(0.0, 0.0, 1.0),
            viewport: vec4(viewport_width, viewport_height, 0.0, 1.0),

            view_matrix: Mat4::IDENTITY,
            projection_matrix: Mat4::IDENTITY,
            combined_matrix: Mat4::IDENTITY,
        }
    }

    pub fn get_position(&self) -> Vec2 {
        vec2(self.position.x, self.position.y)
    }

    pub fn set_position(&mut self, position: Vec2) {
        self.position.x = position.x;
        self.position.y = position.y;
    }

    pub fn get_zoom(&self) -> f32 {
        self.position.z
    }

    pub fn set_zoom(&mut self, zoom: f32) {
        self.position.z = zoom;
    }

    pub fn get_viewport(&self) -> Vec2 {
        vec2(self.viewport.x, self.viewport.y)
    }

    pub fn set_viewport(&mut self, viewport_width: f32, viewport_height: f32) {
        self.viewport.x = viewport_width;
        self.viewport.y = viewport_height;
    }

    pub fn get_view_matrix(&self) -> Mat4 {
        self.view_matrix
    }

    pub fn get_projection_matrix(&self) -> Mat4 {
        self.projection_matrix
    }

    pub fn get_combined_matrix(&self) -> Mat4 {
        self.combined_matrix
    }

    pub fn update(&mut self) {
        let zoom = self.position.z;
        let eye = vec3(self.position.x, self.position.y, 0.0);
        let dir = vec3(0.0, 0.0, -1.0);
        let up = vec3(0.0, 1.0, 0.0);

        self.view_matrix = Mat4::look_at_rh(eye, eye + dir, up);
        self.projection_matrix = Mat4::orthographic_rh(
            (self.viewport.x / -2.0) * zoom,
            (self.viewport.x / 2.0) * zoom,
            (self.viewport.y / -2.0) * zoom,
            (self.viewport.y / 2.0) * zoom,
            self.viewport.z,
            self.viewport.w,
        );
        self.combined_matrix = self.projection_matrix * self.view_matrix;
    }
}

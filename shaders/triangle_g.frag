#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 frag_position;
layout(location = 1) in vec3 frag_color;
layout(location = 2) in vec2 frag_uvs;

layout(binding = 0) uniform sampler2D texture_sampler;

layout(location = 0) out vec4 out_color;

void main() {
  vec4 sampled_color = texture(texture_sampler, frag_uvs);
  sampled_color.rgb *= frag_color;

  out_color = sampled_color;
}

use crate::vulkan::sprite_batch::SpriteBatch;
use anyhow::Result;
use app_core::{
    application::{init_application, Application},
    event::Event,
};
use ash::*;
use bevy_ecs::prelude::*;
use components::{
    CameraTagComponent, ControlComponent, PositionComponent, SpriteComponent, VelocityComponent,
};
use gfx::camera::OrthoCamera;
use glam::*;
use std::sync::{Arc, Mutex};
use systems::{
    camera_tag_system, control_system, render_system, sprite_position_system, velocity_system,
};
use vulkan::{sprite_batch::Sprite, texture::Texture, texture_region::TextureRegion};

mod aabb;
mod app_core;
mod components;
mod gfx;
mod systems;
mod vulkan;

struct Volkus {
    sprite_batch: Arc<Mutex<SpriteBatch>>,

    framebuffers: Vec<vk::Framebuffer>,
    command_pool: vk::CommandPool,
    command_buffer: vk::CommandBuffer,
    image_available_semaphore: vk::Semaphore,
    render_finished_semaphore: vk::Semaphore,

    world: World,
    render_schedule: Schedule,
    update_schedule: Schedule,
}

impl Application for Volkus {
    fn new(context: Arc<app_core::context::Context>) -> Result<Self> {
        let vk_context = context.vulkan_context.clone();

        let sprite_batch = SpriteBatch::new(1000, vk_context.clone())?;
        let camera = OrthoCamera::new(1920.0, 1080.0);

        let framebuffers = vk_context
            .swapchain
            .image_views
            .iter()
            .map(|image_view| {
                let attachments = vec![*image_view];

                let framebuffer_create_info = vk::FramebufferCreateInfo::builder()
                    .render_pass(sprite_batch.lock().unwrap().get_render_pass())
                    .attachments(&attachments)
                    .width(vk_context.swapchain.extent.width)
                    .height(vk_context.swapchain.extent.height)
                    .layers(1);

                unsafe {
                    vk_context
                        .device
                        .logical_device
                        .create_framebuffer(&framebuffer_create_info, None)
                        .unwrap()
                }
            })
            .collect::<Vec<_>>();

        let command_pool_create_info = vk::CommandPoolCreateInfo::builder()
            .flags(vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
            .queue_family_index(vk_context.device.graphics_queue_family_index);

        let command_pool = unsafe {
            vk_context
                .device
                .logical_device
                .create_command_pool(&command_pool_create_info, None)?
        };

        let command_buffer_allocate_info = vk::CommandBufferAllocateInfo::builder()
            .command_pool(command_pool)
            .level(vk::CommandBufferLevel::PRIMARY)
            .command_buffer_count(1);

        let command_buffer = unsafe {
            vk_context
                .device
                .logical_device
                .allocate_command_buffers(&command_buffer_allocate_info)?
        };

        let semaphore_create_info = vk::SemaphoreCreateInfo::default();

        let image_available_semaphore = unsafe {
            vk_context
                .device
                .logical_device
                .create_semaphore(&semaphore_create_info, None)?
        };
        let render_finished_semaphore = unsafe {
            vk_context
                .device
                .logical_device
                .create_semaphore(&semaphore_create_info, None)?
        };

        let mut world = World::new();
        world.insert_resource(sprite_batch.clone());
        world.insert_resource(0.0 as f32);
        world.insert_resource(context.clone());
        world.insert_resource(camera);

        let mut render_schedule = Schedule::default();
        render_schedule.add_stage("render", SystemStage::parallel().with_system(render_system));

        let mut update_schedule = Schedule::default();
        update_schedule.add_stage(
            "control",
            SystemStage::parallel().with_system(control_system),
        );
        update_schedule.add_stage_after(
            "control",
            "velocity",
            SystemStage::parallel().with_system(velocity_system),
        );
        update_schedule.add_stage_after(
            "velocity",
            "sprite_update",
            SystemStage::parallel().with_system(sprite_position_system),
        );
        update_schedule.add_stage_after(
            "velocity",
            "camera_tag",
            SystemStage::parallel().with_system(camera_tag_system),
        );

        let texture = Texture::new_from_file(context.vulkan_context.clone(), "textures/test.png")?;

        world
            .spawn()
            .insert(SpriteComponent {
                sprite: Sprite::new(TextureRegion::new(texture.clone())),
            })
            .insert(PositionComponent {
                pos: vec2(0.0, 0.0),
            })
            .insert(VelocityComponent {
                vel: vec2(0.0, 0.0),
            })
            .insert(ControlComponent {})
            .insert(CameraTagComponent {});

        world
            .spawn()
            .insert(SpriteComponent {
                sprite: Sprite::new(TextureRegion::new(texture.clone())),
            })
            .insert(PositionComponent {
                pos: vec2(0.0, 0.0),
            })
            .insert(VelocityComponent {
                vel: vec2(0.0, 0.0),
            });

        Ok(Self {
            sprite_batch,
            framebuffers,
            command_pool,
            command_buffer: command_buffer[0],
            image_available_semaphore,
            render_finished_semaphore,
            world,
            render_schedule,
            update_schedule,
        })
    }

    fn init(&mut self, _context: Arc<app_core::context::Context>) -> Result<()> {
        Ok(())
    }

    fn render(&mut self, context: Arc<app_core::context::Context>, delta_time: f32) -> Result<()> {
        self.world.insert_resource(delta_time);

        let vk_context = context.vulkan_context.clone();

        let (image_index, _) = vk_context
            .swapchain
            .acquire_next_image(self.image_available_semaphore, vk::Fence::null());

        let command_buffer_begin_info = vk::CommandBufferBeginInfo::default();

        unsafe {
            vk_context
                .device
                .logical_device
                .begin_command_buffer(self.command_buffer, &command_buffer_begin_info)
                .unwrap();
        }

        let viewport_info = vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: vk_context.swapchain.extent.width as f32,
            height: vk_context.swapchain.extent.height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        };

        let viewports = vec![viewport_info];

        let scissor_region = vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: vk_context.swapchain.extent,
        };

        let scissor_regions = vec![scissor_region];

        unsafe {
            vk_context.device.logical_device.cmd_set_scissor(
                self.command_buffer,
                0,
                &scissor_regions,
            );
            vk_context
                .device
                .logical_device
                .cmd_set_viewport(self.command_buffer, 0, &viewports);
        }

        let clear_values = vec![vk::ClearValue {
            color: vk::ClearColorValue {
                float32: [0.0, 0.0, 0.0, 1.0],
            },
        }];

        let render_pass_begin_info = vk::RenderPassBeginInfo::builder()
            .render_pass(self.sprite_batch.lock().unwrap().get_render_pass())
            .framebuffer(self.framebuffers[image_index as usize])
            .render_area(vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: vk_context.swapchain.extent,
            })
            .clear_values(&clear_values);

        unsafe {
            vk_context.device.logical_device.cmd_begin_render_pass(
                self.command_buffer,
                &render_pass_begin_info,
                vk::SubpassContents::INLINE,
            );

            self.update_schedule.run(&mut self.world);

            let mut camera = self.world.get_resource_mut::<OrthoCamera>().unwrap();
            camera.update();

            self.sprite_batch
                .lock()
                .unwrap()
                .begin(camera.get_combined_matrix());

            self.render_schedule.run(&mut self.world);

            self.sprite_batch.lock().unwrap().end(self.command_buffer);

            vk_context
                .device
                .logical_device
                .cmd_end_render_pass(self.command_buffer);
            vk_context
                .device
                .logical_device
                .end_command_buffer(self.command_buffer)
                .unwrap();
        }

        let wait_semaphores = vec![self.image_available_semaphore];
        let wait_stages = vec![vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT];
        let command_buffers = vec![self.command_buffer];
        let signal_semaphores = vec![self.render_finished_semaphore];

        let submit_info = vk::SubmitInfo::builder()
            .wait_semaphores(&wait_semaphores)
            .wait_dst_stage_mask(&wait_stages)
            .command_buffers(&command_buffers)
            .signal_semaphores(&signal_semaphores)
            .build();

        let submit_infos = vec![submit_info];

        unsafe {
            vk_context
                .device
                .logical_device
                .queue_submit(
                    vk_context.device.graphics_queue,
                    &submit_infos,
                    vk::Fence::null(),
                )
                .unwrap();
        }

        let swapchains = vec![vk_context.swapchain.swapchain_handle];
        let image_indices = vec![image_index];

        let present_info = vk::PresentInfoKHR::builder()
            .wait_semaphores(&signal_semaphores)
            .swapchains(&swapchains)
            .image_indices(&image_indices);

        vk_context
            .swapchain
            .queue_present(vk_context.device.graphics_queue, &present_info);

        unsafe {
            vk_context
                .device
                .logical_device
                .queue_wait_idle(vk_context.device.graphics_queue)
                .unwrap();
        }

        Ok(())
    }

    fn on_event(&mut self, _context: Arc<app_core::context::Context>, _event: Event) -> Result<()> {
        Ok(())
    }
}

fn main() -> Result<()> {
    let mut config = app_core::config::Config::default();
    config.window_width = 1920;
    config.window_height = 1080;
    config.enable_validation = false;

    init_application::<Volkus>(config)?;

    Ok(())
}

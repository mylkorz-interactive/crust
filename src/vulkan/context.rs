use crate::app_core::config::Config;
use crate::app_core::window::Window;
use crate::vulkan::device::Device;
use crate::vulkan::swapchain::Swapchain;
use crate::vulkan::VulkanError;
use ash::{
    extensions::ext::DebugUtils,
    vk::{self, DebugUtilsMessengerEXT},
    Entry, Instance,
};
use log::*;
use std::borrow::Cow;
use std::ffi::{CStr, CString};
use vk_mem::*;

pub struct Context {
    pub entry: Entry,
    pub instance: Instance,
    pub debug_utils_loader: Option<DebugUtils>,
    pub debug_callback: Option<DebugUtilsMessengerEXT>,
    pub swapchain: Swapchain,
    pub device: Device,
    pub memory_allocator: Allocator,
}

unsafe extern "system" fn validation_layer_callback(
    message_severity: vk::DebugUtilsMessageSeverityFlagsEXT,
    message_type: vk::DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const vk::DebugUtilsMessengerCallbackDataEXT,
    _user_data: *mut std::os::raw::c_void,
) -> vk::Bool32 {
    let callback_data = *p_callback_data;
    let message_id_number: i32 = callback_data.message_id_number as i32;

    let message_id_name = if callback_data.p_message_id_name.is_null() {
        Cow::from("")
    } else {
        CStr::from_ptr(callback_data.p_message_id_name).to_string_lossy()
    };

    let message = if callback_data.p_message.is_null() {
        Cow::from("")
    } else {
        CStr::from_ptr(callback_data.p_message).to_string_lossy()
    };

    let output_message = format!(
        "{:?}:\n{:?} [{} ({})] : {}\n",
        message_severity,
        message_type,
        message_id_name,
        &message_id_number.to_string(),
        message,
    );

    match message_severity {
        vk::DebugUtilsMessageSeverityFlagsEXT::ERROR => {
            error!("{}", output_message);
        }
        vk::DebugUtilsMessageSeverityFlagsEXT::WARNING => {
            warn!("{}", output_message);
        }
        vk::DebugUtilsMessageSeverityFlagsEXT::INFO => {
            info!("{}", output_message);
        }
        vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE => {
            trace!("{}", output_message);
        }
        _ => {
            warn!(
                "Unknown validation layer message severity type: {:?}",
                message_severity
            );
        }
    }

    vk::FALSE
}

impl Context {
    pub fn new(window: &Window, config: &Config) -> Result<Self, VulkanError> {
        let entry = unsafe { Entry::load().map_err(|e| VulkanError::LoadError(e.to_string()))? };

        let mut validation_layer_names =
            vec![unsafe { CStr::from_bytes_with_nul_unchecked(b"VK_LAYER_KHRONOS_validation\0") }];

        let use_validation_layers = {
            if config.enable_validation {
                let layer_properties = entry
                    .enumerate_instance_layer_properties()
                    .map_err(|e| VulkanError::LoadError(e.to_string()))?;
                let available_layer_names = layer_properties
                    .iter()
                    .map(|layer| unsafe { CStr::from_ptr(layer.layer_name.as_ptr()) })
                    .collect::<Vec<_>>();

                if validation_layer_names
                    .iter()
                    .all(|layer| available_layer_names.contains(layer))
                {
                    info!("** Using Vulkan validation layers **");
                    true
                } else {
                    warn!("Validation layers were requested, but not supported");
                    false
                }
            } else {
                false
            }
        };

        if !use_validation_layers {
            validation_layer_names.clear();
        }

        let validation_layer_names = validation_layer_names
            .into_iter()
            .map(|layer| layer.as_ptr())
            .collect::<Vec<_>>();

        info!("Creating Vulkan instance");
        let instance = {
            let application_name =
                CString::new("Wulcano").map_err(|e| VulkanError::LoadError(e.to_string()))?;
            let engine_name = CString::new("JavaScriptIsBest")
                .map_err(|e| VulkanError::LoadError(e.to_string()))?;

            let surface_extensions =
                ash_window::enumerate_required_extensions(window.get_window_handle())
                    .map_err(|e| VulkanError::LoadError(e.to_string()))?;
            let mut instance_extensions = surface_extensions
                .into_iter()
                .map(|extension| extension.as_ptr())
                .collect::<Vec<_>>();

            if use_validation_layers {
                instance_extensions.push(DebugUtils::name().as_ptr());
            }

            let application_info = vk::ApplicationInfo::builder()
                .application_name(application_name.as_c_str())
                .engine_name(engine_name.as_c_str())
                .application_version(vk::make_api_version(1, 0, 0, 0))
                .engine_version(vk::make_api_version(1, 0, 0, 0))
                .api_version(vk::make_api_version(0, 1, 2, 0));

            let instance_create_info = vk::InstanceCreateInfo::builder()
                .application_info(&application_info)
                .enabled_extension_names(&instance_extensions)
                .enabled_layer_names(&validation_layer_names);

            unsafe {
                entry
                    .create_instance(&instance_create_info, None)
                    .map_err(|e| VulkanError::LoadError(e.to_string()))?
            }
        };

        let mut debug_utils_loader: Option<DebugUtils> = None;
        let mut debug_callback: Option<vk::DebugUtilsMessengerEXT> = None;

        if use_validation_layers {
            info!("Setting up validation layers");
            let debug_messenger_create_info = vk::DebugUtilsMessengerCreateInfoEXT::builder()
                .message_severity(
                    vk::DebugUtilsMessageSeverityFlagsEXT::ERROR
                        | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING,
                )
                .message_type(
                    vk::DebugUtilsMessageTypeFlagsEXT::GENERAL
                        | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION
                        | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE,
                )
                .pfn_user_callback(Some(validation_layer_callback));
            debug_utils_loader = Some(DebugUtils::new(&entry, &instance));
            debug_callback = Some(unsafe {
                debug_utils_loader
                    .as_ref()
                    .expect("Something went wrong creating debug_utils_loader")
                    .create_debug_utils_messenger(&debug_messenger_create_info, None)
                    .map_err(|e| VulkanError::LoadError(e.to_string()))?
            });
        }

        info!("Selecting physical device");
        let physical_devices = unsafe {
            instance
                .enumerate_physical_devices()
                .map_err(|e| VulkanError::LoadError(e.to_string()))?
        };

        let mut available_physical_devices = String::from("");

        for physical_device in &physical_devices {
            let physical_device_properties =
                unsafe { instance.get_physical_device_properties(*physical_device) };

            available_physical_devices.push_str(unsafe {
                CStr::from_ptr(physical_device_properties.device_name.as_ptr())
                    .to_string_lossy()
                    .into_owned()
                    .as_str()
            });
        }
        info!(
            "Available physical devices:
    {}",
            available_physical_devices
        );

        let physical_device = physical_devices
            .into_iter()
            .max_by_key(|physical_device| {
                match unsafe { instance.get_physical_device_properties(*physical_device) }
                    .device_type
                {
                    vk::PhysicalDeviceType::DISCRETE_GPU => 1,
                    _ => 0,
                }
            })
            .expect("Could not find a physical device");

        let physical_device_properties =
            unsafe { instance.get_physical_device_properties(physical_device) };

        info!("Selected physical device: {:?}", unsafe {
            CStr::from_ptr(physical_device_properties.device_name.as_ptr())
        });

        info!("Creating present surface");
        let surface_loader = ash::extensions::khr::Surface::new(&entry, &instance);
        let surface = unsafe {
            ash_window::create_surface(&entry, &instance, window.get_window_handle(), None)
                .map_err(|e| VulkanError::SwapchainError(e.to_string()))?
        };

        let device = Device::new(
            &instance,
            physical_device,
            &surface_loader,
            &surface,
            &validation_layer_names,
        )?;

        let swapchain = Swapchain::new(&instance, window, &device, surface_loader, surface)?;

        let memory_allocator_info = AllocatorCreateInfo {
            physical_device,
            device: device.logical_device.clone(),
            instance: instance.clone(),
            flags: AllocatorCreateFlags::NONE, // TODO: take into account support for DEDICATED_ALLOCATIONS
            preferred_large_heap_block_size: 0, // defaults to 256MiB
            frame_in_use_count: 0,             // TODO: look into this
            heap_size_limits: None,
            allocation_callbacks: None,
            vulkan_api_version: 0,
        };

        let memory_allocator = unsafe {
            Allocator::new(&memory_allocator_info)
                .map_err(|e| VulkanError::LoadError(e.to_string()))?
        };

        Ok(Context {
            entry,
            instance,
            debug_utils_loader,
            debug_callback,
            device,
            swapchain,
            memory_allocator,
        })
    }
}
